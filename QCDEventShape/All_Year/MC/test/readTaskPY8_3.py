import re
import argparse

# Function to process the file and print job numbers
def process_file(file_path):
    # List of job numbers to search for
    #job_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    job_numbers = [15, 30, 50, 80, 120, 170, 300, 470, 600, 800, 1000, 1400, 1800, 2400, 3200]

    # Open the file for reading
    with open(file_path, 'r') as file:
        # Initialize a counter
        i = 0
        # Iterate over each line in the file
        for line in file:
            # Check if the line contains 'Task name'
            if 'Task name' in line:
                # Use regular expression to find the pattern 'ddmmyy_hhmmss'
                match = re.search(r'\b\d{6}_\d{6}\b', line)
                if match:
                    # Extract the task name
                    task_name = match.group()
                    # Check if we have more job numbers to print
                    #if i < len(job_numbers):
                    print("Jobnum"+str(job_numbers[i])+"="+match.group())
                        #print(f"Jobnum{job_numbers[i]}={task_name}")
                        # Increment the counter
                    i += 1
                    #else:
                        #break

# Main function to handle argument parsing
def main():
    # Create argument parser
    parser = argparse.ArgumentParser(description='Process a text file to extract task names.')
    # Add argument for file path
    parser.add_argument('file_path', type=str, help='Path to the text file')

    # Parse the arguments
    args = parser.parse_args()

    # Process the file
    process_file(args.file_path)

# Entry point for the script
if __name__ == '__main__':
    main()

