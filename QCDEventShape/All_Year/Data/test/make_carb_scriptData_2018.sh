#!/bin/bash
model=Data2018
#model=MG

ptval=('A' 'B' 'C' 'D')  #
npt=${#ptval[@]}  #number of value to check


dataset=(
'/JetHT/Run2018A-UL2018_MiniAODv2-v1/MINIAOD'
'/JetHT/Run2018B-UL2018_MiniAODv2-v1/MINIAOD'
'/JetHT/Run2018C-UL2018_MiniAODv2-v1/MINIAOD'
'/JetHT/Run2018D-UL2018_MiniAODv2-v2/MINIAOD')


projectfold=crab_projects_2018_Data

for((i =1 ;i <${npt}+1 ; i++))
do
ipt=${ptval[$i-1]}
iptnext=${ptval[$i]}

cat>Run_bin_auto_${model}_${ipt}.py<<%
import FWCore.ParameterSet.Config as cms
#from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16

process = cms.Process("Test")

## switch to uncheduled mode
#process.options.allowUnscheduled = cms.untracked.bool(True)
#process.Tracer = cms.Service("Tracer")

process.load("PhysicsTools.PatAlgos.producersLayer1.patCandidates_cff")
process.load("PhysicsTools.PatAlgos.selectionLayer1.selectedPatCandidates_cff")


# source
process.source = cms.Source("PoolSource",
fileNames = cms.untracked.vstring(
'/store/data/Run2018A/JetHT/MINIAOD/UL2018_MiniAODv2-v1/260000/00B87525-94D1-C741-9B03-00528106D15A.root',
)
)
#process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(50000) )
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

#process.load("Configuration.StandardSequences.Geometry_cff")
#process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff")
process.load("Configuration.Geometry.GeometryRecoDB_cff")
#process.GlobalTag.globaltag = cms.string('POSTLS170_V5')
process.load("Configuration.StandardSequences.MagneticField_cff")
from Configuration.AlCa.GlobalTag import GlobalTag
#process.GlobalTag = GlobalTag(process.GlobalTag,'94X_dataRun2_ReReco_EOY17_v6')
#process.GlobalTag = GlobalTag(process.GlobalTag,'94X_dataRun2_v11')
process.GlobalTag = GlobalTag(process.GlobalTag,'106X_dataRun2_v35')
from PhysicsTools.PatAlgos.tools.coreTools import *
# produce PAT Layer 1
process.load("PhysicsTools.PatAlgos.patSequences_cff")

process.MessageLogger = cms.Service("MessageLogger",
 cout = cms.untracked.PSet(
  default = cms.untracked.PSet( ## kill all messages in the log  

   limit = cms.untracked.int32(0)
  ),
  FwkJob = cms.untracked.PSet( ## but FwkJob category - those unlimitted  

   limit = cms.untracked.int32(-1)
  )
 ),
 categories = cms.untracked.vstring('FwkJob'),
 destinations = cms.untracked.vstring('cout')
)

# For Pileup JetID 
process.load('RecoJets.JetProducers.PileupJetID_cfi')
from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL18
process.pileupJetIdUpdated = process.pileupJetId.clone(
        jets=cms.InputTag("slimmedJets"),
        inputIsCorrected=True,
        applyJec=False,
        vertexes=cms.InputTag("offlineSlimmedPrimaryVertices"),
        algos = cms.VPSet(_chsalgos_106X_UL18),
    )


process.TFileService=cms.Service("TFileService",
    fileName=cms.string("Test_Data_UL18.root")
)
print "test1"

process.analyzeBasicPat = cms.EDAnalyzer("QCDEventShape",
#       photonSrc = cms.untracked.InputTag("cleanPatPhotons"),
#       electronSrc = cms.untracked.InputTag("cleanPatElectrons"),
#       muonSrc = cms.untracked.InputTag("cleanPatMuons"),
#       tauSrc = cms.untracked.InputTag("cleanPatTaus"),
        jetSrc = cms.InputTag("slimmedJets"),
        metSrc = cms.InputTag("slimmedMETs"),
        genSrc = cms.untracked.InputTag("packedGenParticles"),
        pfSrc = cms.InputTag("packedPFCandidates"),
        bits = cms.InputTag("TriggerResults","","HLT"),
        prescales = cms.InputTag("patTrigger"),
        objects = cms.InputTag("selectedPatTrigger"),
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        bsSrc = cms.InputTag("offlineBeamSpot"),
        genjetSrc = cms.InputTag("slimmedGenJets"),
        pileupSrc =cms.InputTag("slimmedAddPileupInfo"),
        ak5pfJetSrc = cms.InputTag("ak5PFJets"),
        ak5genJetSrc = cms.InputTag("ak5GenJets"),
        evtinfo =cms.InputTag("generator"),
        rho = cms.InputTag('fixedGridRhoAll'),
        LHEEventProductInputTag   = cms.InputTag('externalLHEProducer'),
        LHERunInfoProductInputTag = cms.InputTag('externalLHEProducer'),
        PDFCTEQWeightsInputTag   = cms.InputTag('pdfWeights:CT14'),
        PDFMMTHWeightsInputTag   = cms.InputTag('pdfWeights:MMHT2014lo68cl'),
        PDFNNPDFWeightsInputTag   = cms.InputTag('pdfWeights:NNPDF30'),
        #ak5PFJetCHSSrc    = cms.InputTag("ak5PFJetsCHS")
        RootFileName = cms.untracked.string('pythia8_test_13tev.root'),
        GenJET =  cms.untracked.bool(False),
        HistFill = cms.untracked.bool(True),
        MonteCarlo =  cms.untracked.bool(False),
        ParticleLabel =  cms.untracked.bool(False),
        Reconstruct =cms.untracked.bool(True),
#  EtaRange =  cms.untracked.double(5.0),
#  PtThreshold = cms.untracked.double(12.0),
        EtaRange =  cms.untracked.double(3.0),
        PtThreshold = cms.untracked.double(55.0), #effective is 21
        LeadingPtThreshold = cms.untracked.double(150.0), #effective is 81       
#        scaleFactorsFile = cms.FileInPath('CondFormats/JetMETObjects/data/Summer15_V0_MC_JER_AK4PFchs.txt'),
#        resolutionsFile = cms.FileInPath('CondFormats/JetMETObjects/data/Summer15_V0_MC_JER_AK4PFchs.txt'), 
#        scaleFactorsFile = cms.FileInPath('Fall15_25nsV2_MC_SF_AK4PFchs.txt'),
#        resolutionsFile = cms.FileInPath('Fall15_25nsV2_MC_PtResolution_AK4PFchs.txt'),
#        scaleFactorsFile = cms.FileInPath('Fall15_25nsV2_MC_SF_AK4PFchs.txt'),
#        resolutionsFile = cms.FileInPath('Fall15_25nsV2_MC_PtResolution_AK4PFchs.txt'),
#         JECFile = cms.FileInPath('${jecfile[$i-1]}'),


 )

#process.ak5PFJets = ak5PFJets.clone(src = 'packedPFCandidates')
#process.analyzeBasicPat.append("keep *_ak5PFJets_*_EX")

#process.analyzeBasicPat.append("keep *_ak5PFJetsCHS_*_EX")
process.p = cms.Path(process.pileupJetIdUpdated)
process.p = cms.Path(process.analyzeBasicPat)
#process.p = cms.Path(process.analyzeBasicPat*process.pileupJetIdUpdated)
print "test2"
#process.p = cms.Path(process.ak5PFJets*process.ak5GenJets*process.analyzeBasicPat)


%







cat>crab_bin_auto_${model}_${ipt}.py<<%
#from CRABClient.UserUtilities import config, getUsernameFromSiteDB
from CRABClient.UserUtilities import config
config = config()

config.General.requestName ='ESVQCD_UL18_Data_${ipt}'

config.General.workArea = '$projectfold'

config.General.transferOutputs = True
config.General.transferLogs = True
config.JobType.pluginName = 'Analysis'
config.JobType.allowUndistributedCMSSW = True
config.JobType.psetName = 'Run_bin_auto_${model}_${ipt}.py'


config.JobType.inputFiles= [
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_JRV2_MC_SF_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_JRV2_MC_PtResolution_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_V5_MC_Uncertainty_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunA_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunB_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunC_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunD_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunA_V5_DATA_Uncertainty_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunB_V5_DATA_Uncertainty_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunC_V5_DATA_Uncertainty_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunD_V5_DATA_Uncertainty_AK4PFchs.txt"
]




config.Data.inputDataset ='${dataset[$i-1]}'

config.Data.inputDBS = 'global'
config.Data.splitting = 'LumiBased'
config.Data.unitsPerJob = 15
config.Data.lumiMask ='HLT_13TeV_UL2018_Collisions18_GoldenJSON.txt'


#config.Data.outLFNDirBase = '/store/user/%s/' % (getUsernameFromSiteDB())
config.Data.publication = False
#config.Data.publishDataName = 'May2015_Data_analysis'

config.Site.storageSite = 'T2_IN_TIFR'



%
done

cat>crab_submit_auto_${model}_${projectfold}.sh<<%
crab submit -c crab_bin_auto_${model}_A.py
crab submit -c crab_bin_auto_${model}_B.py
crab submit -c crab_bin_auto_${model}_C.py
crab submit -c crab_bin_auto_${model}_D.py

%

cat>crab_status_auto_${model}_${projectfold}.sh<<%
crab status -d $projectfold/crab_ESVQCD_UL18_Data_A --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL18_Data_B --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL18_Data_C --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL18_Data_D --verboseErrors
%

cat>crab_resubmit_auto_${model}_${projectfold}.sh<<%
crab resubmit -d $projectfold/crab_ESVQCD_UL18_Data_A 
crab resubmit -d $projectfold/crab_ESVQCD_UL18_Data_B 
crab resubmit -d $projectfold/crab_ESVQCD_UL18_Data_C 
crab resubmit -d $projectfold/crab_ESVQCD_UL18_Data_D 
%


cat>crab_kill_auto_${model}_${projectfold}.sh<<%
crab kill -d $projectfold/crab_ESVQCD_UL18_Data_A
crab kill -d $projectfold/crab_ESVQCD_UL18_Data_B
crab kill -d $projectfold/crab_ESVQCD_UL18_Data_C
crab kill -d $projectfold/crab_ESVQCD_UL18_Data_D
%

chmod 755 crab_resubmit_auto_${model}_${projectfold}.sh
chmod 755 crab_status_auto_${model}_${projectfold}.sh
chmod 755 crab_submit_auto_${model}_${projectfold}.sh
chmod 755 crab_kill_auto_${model}_${projectfold}.sh

#./crab_submit_auto_${model}_${projectfold}.sh
