#!/bin/bash

# Check if the script name is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <script_to_run>"
    exit 1
fi

SCRIPT_TO_RUN=$1

# Run the specified script and append output to tasklist.txt
./$SCRIPT_TO_RUN >> tasklist.txt

# Run the Python script with tasklist.txt as an argument
python readTaskPY8_3.py tasklist.txt

# Remove the tasklist.txt file after processing
rm tasklist.txt

