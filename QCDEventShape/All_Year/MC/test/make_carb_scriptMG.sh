#!/bin/bash
#model=py8
model=MG

#ptval=(15 30 50 80 120 170 300 470 600 800 1000 1400 1800 2400 3200 'inf')  #
ptval=(50 100 200 300 500 700 1000 1500 2000 'inf')  #
npt=${#ptval[@]}  #number of value to check


dataset=(
'/QCD_HT50to100_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
'/QCD_HT100to200_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
'/QCD_HT200to300_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
'/QCD_HT300to500_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
'/QCD_HT500to700_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
'/QCD_HT700to1000_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'
'/QCD_HT1000to1500_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
'/QCD_HT1500to2000_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
'/QCD_HT2000toInf_TuneCP5_PSWeights_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM')





#dataset=('/QCD_HT50to100_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v1/MINIAODSIM'
#         '/QCD_HT100to200_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v1/MINIAODSIM'
#         '/QCD_HT200to300_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v1/MINIAODSIM'
#         '/QCD_HT300to500_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v1/MINIAODSIM'
#         '/QCD_HT500to700_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v1/MINIAODSIM'
#         '/QCD_HT700to1000_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v1/MINIAODSIM'
#         '/QCD_HT1000to1500_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v1/MINIAODSIM'
#         '/QCD_HT1500to2000_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v1/MINIAODSIM'
#         '/QCD_HT2000toInf_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v1/MINIAODSIM')


projectfold=crab_projects_18bin_mg_eta21

for((i =1 ;i <${npt} ; i++))
do
ipt=${ptval[$i-1]}
iptnext=${ptval[$i]}

cat>crab_bin_auto_${model}_${ipt}_$iptnext.py<<%
#from CRABClient.UserUtilities import config, getUsernameFromSiteDB
from CRABClient.UserUtilities import config
config = config()

config.General.requestName ='ESVQCD_MG_${ipt}to${iptnext}_tuneCP5_bin'

config.General.workArea = '$projectfold'



config.General.transferOutputs = True
config.General.transferLogs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'Run_QCD_test_MiniAOD_102x_mc_cfg.py'

#config.JobType.maxMemoryMB = 9000 # Default is 2500 : Max I have used is 13000
#config.JobType.maxJobRuntimeMin = 2750 #Default is 1315; 2750 minutes guaranteed to be available; Max I have used is 9000
#config.JobType.numCores = 4




config.JobType.inputFiles= [
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_JRV2_MC_SF_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_JRV2_MC_PtResolution_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_V5_MC_Uncertainty_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunA_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunB_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunC_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunD_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunA_V5_DATA_Uncertainty_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunB_V5_DATA_Uncertainty_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunC_V5_DATA_Uncertainty_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2018/AK4PFCHS_Summer19UL/Summer19UL18_RunD_V5_DATA_Uncertainty_AK4PFchs.txt"
]




config.Data.inputDataset ='${dataset[$i-1]}'

config.Data.inputDBS = 'global'
#config.Data.splitting = 'EventBased'
#config.Data.splitting = 'LumiBased'
config.Data.splitting = 'FileBased'
#config.Data.splitting = 'Automatic'
#config.Data.unitsPerJob = 10  # for Automatic must be 180-2700 range
config.Data.unitsPerJob = 1  #For Filebased or Lumibased
#config.Data.outLFNDirBase = '/store/user/%s/' % (getUsernameFromSiteDB())
#config.Data.outLFNDirBase = '/store/user/%s/' % (sukundu)
config.Data.publication = True
config.Data.outputDatasetTag = 'MC_MG_UL_2018'
config.JobType.allowUndistributedCMSSW = True
config.Site.storageSite ='T2_IN_TIFR'

%
done

cat>crab_submit_auto_${model}_${projectfold}.sh<<%
crab submit -c crab_bin_auto_MG_50_100.py
crab submit -c crab_bin_auto_MG_100_200.py
crab submit -c crab_bin_auto_MG_200_300.py
crab submit -c crab_bin_auto_MG_300_500.py
crab submit -c crab_bin_auto_MG_500_700.py
crab submit -c crab_bin_auto_MG_700_1000.py
crab submit -c crab_bin_auto_MG_1000_1500.py
crab submit -c crab_bin_auto_MG_1500_2000.py
crab submit -c crab_bin_auto_MG_2000_inf.py
%

cat>crab_status_auto_${model}_${projectfold}.sh<<%
crab status -d $projectfold/crab_ESVQCD_MG_50to100_tuneCP5_bin  --verboseErrors
crab status -d $projectfold/crab_ESVQCD_MG_100to200_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_MG_200to300_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_MG_300to500_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_MG_500to700_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_MG_700to1000_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_MG_1000to1500_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_MG_1500to2000_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_MG_2000toinf_tuneCP5_bin --verboseErrors
%

cat>crab_resubmit_auto_${model}_${projectfold}.sh<<%
crab resubmit -d $projectfold/crab_ESVQCD_MG_50to100_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_MG_100to200_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_MG_200to300_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_MG_300to500_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_MG_500to700_tuneCP5_bin
crab resubmit -d $projectfold/crab_ESVQCD_MG_700to1000_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_MG_1000to1500_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_MG_1500to2000_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_MG_2000toinf_tuneCP5_bin 

%
cat>crab_kill_auto_${model}_${projectfold}.sh<<%
crab kill -d $projectfold/crab_ESVQCD_MG_50to100_tuneCP5_bin  
crab kill -d $projectfold/crab_ESVQCD_MG_100to200_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_MG_200to300_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_MG_300to500_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_MG_500to700_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_MG_700to1000_tuneCP5_bin
crab kill -d $projectfold/crab_ESVQCD_MG_1000to1500_tuneCP5_bin
crab kill -d $projectfold/crab_ESVQCD_MG_1500to2000_tuneCP5_bin
crab kill -d $projectfold/crab_ESVQCD_MG_2000toinf_tuneCP5_bin
%

chmod 755 crab_resubmit_auto_${model}_${projectfold}.sh
chmod 755 crab_status_auto_${model}_${projectfold}.sh
chmod 755 crab_submit_auto_${model}_${projectfold}.sh
chmod 755 crab_kill_auto_${model}_${projectfold}.sh

./crab_submit_auto_${model}_${projectfold}.sh
