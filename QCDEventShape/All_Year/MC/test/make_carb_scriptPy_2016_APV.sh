#!/bin/bash
model=PYTHIA8_2016_APV_L1Pre_DN
#model=MG

ptval=(15 30 50 80 120 170 300 470 600 800 1000 1400 1800 2400 3200 'inf')  #
#ptval=(50 100 200 300 500 700 1000 1500 2000 'inf')  #
npt=${#ptval[@]}  #number of value to check

dataset=(
'/QCD_Pt_15to30_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_30to50_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_50to80_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_80to120_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_120to170_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_170to300_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_300to470_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_470to600_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_600to800_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_1000to1400_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_1400to1800_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_1800to2400_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_2400to3200_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
'/QCD_Pt_3200toInf_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPVv2-106X_mcRun2_asymptotic_preVFP_v11-v1/MINIAODSIM'
)


#dataset=(
#        '/QCD_Pt_15to30_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_30to50_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_50to80_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_80to120_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_120to170_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_170to300_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_300to470_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_470to600_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_600to800_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_1000to1400_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_1400to1800_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_1800to2400_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_2400to3200_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM'
#        '/QCD_Pt_3200toInf_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAODAPV-106X_mcRun2_asymptotic_preVFP_v8-v2/MINIAODSIM' )


#dataset=('/QCD_Pt_15to30_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_30to50_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_50to80_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_80to120_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_120to170_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_170to300_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_300to470_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_470to600_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_600to800_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_1000to1400_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_1400to1800_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_1800to2400_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_2400to3200_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM'
#          '/QCD_Pt_3200toInf_TuneCP5_13TeV_pythia8/RunIISummer20UL16MiniAOD-106X_mcRun2_asymptotic_v13-v2/MINIAODSIM')


projectfold=crab_projects_${model}

for((i =1 ;i <${npt} ; i++))
do
ipt=${ptval[$i-1]}
iptnext=${ptval[$i]}

cat>Run_bin_auto_${model}_${ipt}_${iptnext}.py<<%
import FWCore.ParameterSet.Config as cms

process = cms.Process("Test")

## switch to uncheduled mode
#process.options.allowUnscheduled = cms.untracked.bool(True)
#process.Tracer = cms.Service("Tracer")

process.load("PhysicsTools.PatAlgos.producersLayer1.patCandidates_cff")
process.load("PhysicsTools.PatAlgos.selectionLayer1.selectedPatCandidates_cff")


process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
'/store/mc/RunIISummer20UL16MiniAODAPVv2/QCD_Pt_50to80_TuneCP5_13TeV_pythia8/MINIAODSIM/106X_mcRun2_asymptotic_preVFP_v11-v1/120000/2536CFB8-25BF-8C45-9359-F3D51CBB002E.root',
 )
#eventsToSkip = cms.untracked.VEventRange('1:1950-1:2000'),
#eventsToSkip = cms.untracked.EventRange('1:1950-1:2000'),
#eventRanges = cms.untracked.VEventRange('1:1000-1:2000'),
)

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )
#process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(50000) )

#process.load("Configuration.StandardSequences.Geometry_cff")
#process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff")
process.load("Configuration.Geometry.GeometryRecoDB_cff")

#process.GlobalTag.globaltag = cms.string('POSTLS170_V5')
process.load("Configuration.StandardSequences.MagneticField_cff")

from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag,'106X_mcRun2_asymptotic_preVFP_v11')
from PhysicsTools.PatAlgos.tools.coreTools import *
# produce PAT Layer 1
process.load("PhysicsTools.PatAlgos.patSequences_cff")

#process.load("HLTrigger.HLTcore.hltPrescaleRecorder_cfi")


process.options = cms.untracked.PSet(

)

#Setup FWK for multithreaded
#process.options.numberOfThreads=cms.untracked.uint32(1)
#process.options.numberOfStreams=cms.untracked.uint32(0)

process.MessageLogger = cms.Service("MessageLogger",
 cout = cms.untracked.PSet(
  default = cms.untracked.PSet( ## kill all messages in the log  

   limit = cms.untracked.int32(0)
  ),
  FwkJob = cms.untracked.PSet( ## but FwkJob category - those unlimitted  

   limit = cms.untracked.int32(-1)
  )
 ),
 categories = cms.untracked.vstring('FwkJob'),
 destinations = cms.untracked.vstring('cout')
)

# For Pileup JetID 
process.load('RecoJets.JetProducers.PileupJetID_cfi')
from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16
process.pileupJetIdUpdated = process.pileupJetId.clone(
        jets=cms.InputTag("slimmedJets"),
        inputIsCorrected=True,
        applyJec=False,
        vertexes=cms.InputTag("offlineSlimmedPrimaryVertices"),
        algos = cms.VPSet(_chsalgos_106X_UL16),
    )


process.TFileService=cms.Service("TFileService",
    fileName=cms.string("Test_MC_QCD.root")
)
print "test1"


#For L1 Prefire
from PhysicsTools.PatUtils.l1PrefiringWeightProducer_cfi import l1PrefiringWeightProducer
process.prefiringweight = l1PrefiringWeightProducer.clone(
    TheJets = cms.InputTag("slimmedJets"), #this should be the slimmedJets collection with up to date JECs !
#   L1Maps = cms.string("L1PrefiringMaps.root"),
    DataEraECAL = cms.string("UL2016preVFP"),
    DataEraMuon = cms.string("2016preVFP"),
    UseJetEMPt = cms.bool(False),
    #PrefiringRateSystematicUncty = cms.double(0.2),
    PrefiringRateSystematicUnctyECAL = cms.double(0.2),
    PrefiringRateSystematicUnctyMuon = cms.double(0.2),

)

# Produce PDF weights (maximum is 3)
process.pdfWeights = cms.EDProducer("PdfWeightProducer",
# Fix POWHEG if buggy (this PDF set will also appear on output,
# so only two more PDF sets can be added in PdfSetNames if not "")
#FixPOWHEG = cms.untracked.string("cteq66.LHgrid"),
#GenTag = cms.untracked.InputTag("genParticles"),
        PdfInfoTag = cms.untracked.InputTag("generator"),
        PdfSetNames = cms.untracked.vstring(
#               "CT10nlo.LHgrid"
                 "PDF4LHC15_100.LHgrid"
                , "MSTW2008nlo68cl.LHgrid"
                , "NNPDF30_100.LHgrid"
            )
      )

process.analyzeBasicPat = cms.EDAnalyzer("QCDEventShape",
#       photonSrc = cms.untracked.InputTag("cleanPatPhotons"),
#       electronSrc = cms.untracked.InputTag("cleanPatElectrons"),
#       muonSrc = cms.untracked.InputTag("cleanPatMuons"),
#       tauSrc = cms.untracked.InputTag("cleanPatTaus"),
#        r = cms.EventRange('1:1000-1:2000'),
        jetSrc = cms.InputTag("slimmedJets"),
        metSrc = cms.InputTag("slimmedMETs"),
        genSrc = cms.untracked.InputTag("packedGenParticles"),
        pfSrc = cms.InputTag("packedPFCandidates"),
        bits = cms.InputTag("TriggerResults","","HLT"),
        prescales = cms.InputTag("patTrigger"),
        objects = cms.InputTag("selectedPatTrigger"),
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        bsSrc = cms.InputTag("offlineBeamSpot"),
        genjetSrc = cms.InputTag("slimmedGenJets"),
        pileupSrc =cms.InputTag("slimmedAddPileupInfo"),
        ak5pfJetSrc = cms.InputTag("ak5PFJets"),
        ak5genJetSrc = cms.InputTag("ak5GenJets"),
        evtinfo =cms.InputTag("generator"),
        rho = cms.InputTag('fixedGridRhoAll'),
        LHEEventProductInputTag   = cms.InputTag('externalLHEProducer'),
        LHERunInfoProductInputTag = cms.InputTag('externalLHEProducer'),
        PDFCTEQWeightsInputTag   = cms.InputTag('pdfWeights:CT14'),
        PDFMMTHWeightsInputTag   = cms.InputTag('pdfWeights:MMHT2014lo68cl'),
        PDFNNPDFWeightsInputTag   = cms.InputTag('pdfWeights:NNPDF30'),
        metPATSrc = cms.InputTag("TriggerResults","","PAT"),  # Added for met filter
        #ak5PFJetCHSSrc    = cms.InputTag("ak5PFJetsCHS")
        RootFileName = cms.untracked.string('pythia8_test_13tev.root'),
        GenJET =  cms.untracked.bool(True),
        HistFill = cms.untracked.bool(True),
        MonteCarlo =  cms.untracked.bool(True),
        ParticleLabel =  cms.untracked.bool(False),
        Reconstruct =cms.untracked.bool(True),
#  EtaRange =  cms.untracked.double(5.0),
#  PtThreshold = cms.untracked.double(12.0),
        EtaRange =  cms.untracked.double(3.0),
        PtThreshold = cms.untracked.double(55.0), #effective is 21
        LeadingPtThreshold = cms.untracked.double(150.0), #effective is 81       
#        scaleFactorsFile = cms.FileInPath('xxCondFormats/JetMETObjects/data/Summer15_V0_MC_JER_AK4PFchs.txt'),
#        resolutionsFile = cms.FileInPath('xxCondFormats/JetMETObjects/data/Summer15_V0_MC_JER_AK4PFchs.txt'),
#        scaleFactorsFile = cms.FileInPath('Test/QCDEventShape/test/Fall15_25nsV2_MC_SF_AK4PFchs.txt'),
#        resolutionsFile = cms.FileInPath('Test/QCDEventShape/test/Fall15_25nsV2_MC_PtResolution_AK4PFchs.txt'),


 )


#process.analyzeBasicPat.append("keep *_ak5PFJetsCHS_*_EX")
process.p = cms.Path(process.pileupJetIdUpdated)
process.p = cms.Path(process.analyzeBasicPat)

print "test2"
#process.p = cms.Path(process.ak5PFJets*process.ak5GenJets*process.analyzeBasicPat)

%



cat>crab_bin_auto_${model}_${ipt}_$iptnext.py<<%
#from CRABClient.UserUtilities import config, getUsernameFromSiteDB
from CRABClient.UserUtilities import config
config = config()

config.General.requestName ='ESVQCD_UL_Ptbinned_${ipt}to${iptnext}_tuneCP5_bin'
config.General.workArea = '$projectfold'

config.General.transferOutputs = True
config.General.transferLogs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'Run_bin_auto_${model}_${ipt}_${iptnext}.py'

#config.JobType.maxMemoryMB = 9000 # Default is 2500 : Max I have used is 13000
#config.JobType.maxJobRuntimeMin = 2750 #Default is 1315; 2750 minutes guaranteed to be available; Max I have used is 9000
#config.JobType.numCores = 4




config.JobType.inputFiles= [
"/afs/cern.ch/work/s/sobarman/private/suman/Unc/Uncertainty2016/AK4PFCHS_Summer19UL/Summer19UL16APV_RunBCDEF_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sobarman/private/suman/Unc/Uncertainty2016/AK4PFCHS_Summer19UL/Spring16_25nsV10_MC_PtResolution_AK4PFchs.txt",
"/afs/cern.ch/work/s/sobarman/private/suman/Unc/Uncertainty2016/AK4PFCHS_Summer19UL/Spring16_25nsV10_MC_SF_AK4PFchs.txt",
"/afs/cern.ch/work/s/sobarman/private/suman/Unc/Uncertainty2016/AK4PFCHS_Summer19UL/Summer16_25nsV5_MC_Uncertainty_AK4PFchs.txt",
"/afs/cern.ch/work/s/sobarman/private/suman/Unc/Uncertainty2016/AK4PFCHS_Summer20UL/Summer20UL16APV_JRV3_MC_SF_AK4PFchs.txt",
"/afs/cern.ch/work/s/sobarman/private/suman/Unc/Uncertainty2016/AK4PFCHS_Summer20UL/Summer20UL16APV_JRV3_MC_PtResolution_AK4PFchs.txt",
"/afs/cern.ch/work/s/sobarman/private/suman/Unc/Uncertainty2016/AK4PFCHS_Summer20UL/Summer20UL16_JRV3_MC_SF_AK4PFchs.txt",
"/afs/cern.ch/work/s/sobarman/private/suman/Unc/Uncertainty2016/AK4PFCHS_Summer20UL/Summer20UL16_JRV3_MC_PtResolution_AK4PFchs.txt"
]




config.Data.inputDataset ='${dataset[$i-1]}'

config.Data.inputDBS = 'global'
#config.Data.splitting = 'EventBased'
#config.Data.splitting = 'LumiBased'
config.Data.splitting = 'FileBased'
#config.Data.splitting = 'Automatic'
#config.Data.unitsPerJob = 10  # for Automatic must be 180-2700 range
config.Data.unitsPerJob = 1  #For Filebased or Lumibased
#config.Data.outLFNDirBase = '/store/user/%s/' % (getUsernameFromSiteDB())
#config.Data.outLFNDirBase = '/store/user/%s/' % (sukundu)
config.Data.publication = True
config.Data.outputDatasetTag = 'MC_PY82016UL_Bin'
config.JobType.allowUndistributedCMSSW = True
#config.Site.storageSite ='T2_IN_TIFR'
config.Site.storageSite = 'T3_US_FNALLPC'

%
done

cat>crab_submit_auto_${model}_${projectfold}.sh<<%
crab submit -c crab_bin_auto_${model}_15_30.py
crab submit -c crab_bin_auto_${model}_30_50.py
crab submit -c crab_bin_auto_${model}_50_80.py
crab submit -c crab_bin_auto_${model}_80_120.py
crab submit -c crab_bin_auto_${model}_120_170.py
crab submit -c crab_bin_auto_${model}_170_300.py
crab submit -c crab_bin_auto_${model}_300_470.py
crab submit -c crab_bin_auto_${model}_470_600.py
crab submit -c crab_bin_auto_${model}_600_800.py
crab submit -c crab_bin_auto_${model}_800_1000.py
crab submit -c crab_bin_auto_${model}_1000_1400.py
crab submit -c crab_bin_auto_${model}_1400_1800.py
crab submit -c crab_bin_auto_${model}_1800_2400.py
crab submit -c crab_bin_auto_${model}_2400_3200.py
crab submit -c crab_bin_auto_${model}_3200_inf.py


%

cat>crab_status_auto_${model}_${projectfold}.sh<<%
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_15to30_tuneCP5_bin  --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_30to50_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_50to80_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_80to120_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_120to170_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_170to300_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_300to470_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_470to600_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_600to800_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_800to1000_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_1000to1400_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_1400to1800_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_1800to2400_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_2400to3200_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_3200toinf_tuneCP5_bin --verboseErrors
%

cat>crab_resubmit_auto_${model}_${projectfold}.sh<<%
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_15to30_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_30to50_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_50to80_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_80to120_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_120to170_tuneCP5_bin
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_170to300_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_300to470_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_470to600_tuneCP5_bin
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_600to800_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_800to1000_tuneCP5_bin
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_1000to1400_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_1400to1800_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_1800to2400_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_2400to3200_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_3200toinf_tuneCP5_bin 
%


cat>crab_kill_auto_${model}_${projectfold}.sh<<%
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_15to30_tuneCP5_bin  
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_30to50_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_50to80_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_80to120_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_120to170_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_170to300_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_300to470_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_470to600_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_600to800_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_800to1000_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_1000to1400_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_1400to1800_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_1800to2400_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_2400to3200_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_3200toinf_tuneCP5_bin
%

chmod 755 crab_resubmit_auto_${model}_${projectfold}.sh
chmod 755 crab_status_auto_${model}_${projectfold}.sh
chmod 755 crab_submit_auto_${model}_${projectfold}.sh
chmod 755 crab_kill_auto_${model}_${projectfold}.sh

./crab_submit_auto_${model}_${projectfold}.sh
