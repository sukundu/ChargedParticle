#!/bin/bash
model=py8
#model=MG

ptval=(15 30 50 80 120 170 300 470 600 800 1000 1400 1800 2400 3200 'inf')  #
#ptval=(50 100 200 300 500 700 1000 1500 2000 'inf')  #
npt=${#ptval[@]}  #number of value to check


dataset=('/QCD_Pt_15to30_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'               
         '/QCD_Pt_30to50_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_50to80_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_80to120_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_120to170_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_170to300_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_300to470_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_470to600_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_600to800_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_1000to1400_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_1400to1800_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_1800to2400_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_2400to3200_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM'
         '/QCD_Pt_3200toInf_TuneCP5_13TeV_pythia8/RunIISummer20UL17MiniAOD-106X_mc2017_realistic_v6-v2/MINIAODSIM')


projectfold=crab_projects_18bin_py_1Feb22

for((i =1 ;i <${npt} ; i++))
do
ipt=${ptval[$i-1]}
iptnext=${ptval[$i]}

cat>crab_bin_auto_${model}_${ipt}_$iptnext.py<<%
#from CRABClient.UserUtilities import config, getUsernameFromSiteDB
from CRABClient.UserUtilities import config
config = config()

config.General.requestName ='ESVQCD_UL_Ptbinned_${ipt}to${iptnext}_tuneCP5_bin'

config.General.workArea = '$projectfold'



config.General.transferOutputs = True
config.General.transferLogs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'Run_QCD_test_miaod_v2_106x_mc_cfg.py'

#config.JobType.maxMemoryMB = 9000 # Default is 2500 : Max I have used is 13000
#config.JobType.maxJobRuntimeMin = 2750 #Default is 1315; 2750 minutes guaranteed to be available; Max I have used is 9000
#config.JobType.numCores = 4




config.JobType.inputFiles= [
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2017/AK4PFCHS_Summer19UL/Summer19UL17_JRV2_MC_PtResolution_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2017/AK4PFCHS_Summer19UL/Summer19UL17_JRV2_MC_SF_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2017/AK4PFCHS_Summer19UL/Summer19UL17_RunB_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2017/AK4PFCHS_Summer19UL/Summer19UL17_RunC_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2017/AK4PFCHS_Summer19UL/Summer19UL17_RunD_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2017/AK4PFCHS_Summer19UL/Summer19UL17_RunE_V5_DATA_UncertaintySources_AK4PFchs.txt",
"/afs/cern.ch/work/s/sukundu/private/ESV_charge_CMSSW/Uncertainty2017/AK4PFCHS_Summer19UL/Summer19UL17_RunF_V5_DATA_UncertaintySources_AK4PFchs.txt"
]




config.Data.inputDataset ='${dataset[$i-1]}'

config.Data.inputDBS = 'global'
#config.Data.splitting = 'EventBased'
#config.Data.splitting = 'LumiBased'
config.Data.splitting = 'FileBased'
#config.Data.splitting = 'Automatic'
#config.Data.unitsPerJob = 10  # for Automatic must be 180-2700 range
config.Data.unitsPerJob = 1  #For Filebased or Lumibased
#config.Data.outLFNDirBase = '/store/user/%s/' % (getUsernameFromSiteDB())
#config.Data.outLFNDirBase = '/store/user/%s/' % (sukundu)
config.Data.publication = True
config.Data.outputDatasetTag = 'MC_PY82017UL_Bin'
config.JobType.allowUndistributedCMSSW = True
config.Site.storageSite ='T2_IN_TIFR'

%
done

cat>crab_submit_auto_${model}_${projectfold}.sh<<%
crab submit -c crab_bin_auto_py8_15_30.py
crab submit -c crab_bin_auto_py8_30_50.py
crab submit -c crab_bin_auto_py8_50_80.py
crab submit -c crab_bin_auto_py8_80_120.py
crab submit -c crab_bin_auto_py8_120_170.py
crab submit -c crab_bin_auto_py8_170_300.py
crab submit -c crab_bin_auto_py8_300_470.py
crab submit -c crab_bin_auto_py8_470_600.py
crab submit -c crab_bin_auto_py8_600_800.py
crab submit -c crab_bin_auto_py8_800_1000.py
crab submit -c crab_bin_auto_py8_1000_1400.py
crab submit -c crab_bin_auto_py8_1400_1800.py
crab submit -c crab_bin_auto_py8_1800_2400.py
crab submit -c crab_bin_auto_py8_2400_3200.py
crab submit -c crab_bin_auto_py8_3200_inf.py

%

cat>crab_status_auto_${model}_${projectfold}.sh<<%
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_15to30_tuneCP5_bin  --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_30to50_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_50to80_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_80to120_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_120to170_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_170to300_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_300to470_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_470to600_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_600to800_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_800to1000_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_1000to1400_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_1400to1800_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_1800to2400_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_2400to3200_tuneCP5_bin --verboseErrors
crab status -d $projectfold/crab_ESVQCD_UL_Ptbinned_3200toinf_tuneCP5_bin --verboseErrors
%

cat>crab_resubmit_auto_${model}_${projectfold}.sh<<%
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_15to30_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_30to50_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_50to80_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_80to120_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_120to170_tuneCP5_bin
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_170to300_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_300to470_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_470to600_tuneCP5_bin
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_600to800_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_800to1000_tuneCP5_bin
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_1000to1400_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_1400to1800_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_1800to2400_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_2400to3200_tuneCP5_bin 
crab resubmit -d $projectfold/crab_ESVQCD_UL_Ptbinned_3200toinf_tuneCP5_bin 
%


cat>crab_kill_auto_${model}_${projectfold}.sh<<%
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_15to30_tuneCP5_bin  
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_30to50_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_50to80_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_80to120_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_120to170_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_170to300_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_300to470_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_470to600_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_600to800_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_800to1000_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_1000to1400_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_1400to1800_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_1800to2400_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_2400to3200_tuneCP5_bin 
crab kill -d $projectfold/crab_ESVQCD_UL_Ptbinned_3200toinf_tuneCP5_bin
%

chmod 755 crab_resubmit_auto_${model}_${projectfold}.sh
chmod 755 crab_status_auto_${model}_${projectfold}.sh
chmod 755 crab_submit_auto_${model}_${projectfold}.sh
chmod 755 crab_kill_auto_${model}_${projectfold}.sh

./crab_submit_auto_${model}_${projectfold}.sh
